﻿namespace RePers
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.reader_label = new System.Windows.Forms.Label();
            this.reader_combo = new System.Windows.Forms.ComboBox();
            this.path_box = new System.Windows.Forms.TextBox();
            this.path_button = new System.Windows.Forms.Button();
            this.status_box = new System.Windows.Forms.RichTextBox();
            this.main_loop = new System.Windows.Forms.Timer(this.components);
            this.connect_label = new System.Windows.Forms.Label();
            this.fixed_reader_label = new System.Windows.Forms.Label();
            this.refresh_button = new System.Windows.Forms.Button();
            this.folder_select = new System.Windows.Forms.FolderBrowserDialog();
            this.parse_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // reader_label
            // 
            this.reader_label.AutoSize = true;
            this.reader_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.reader_label.Location = new System.Drawing.Point(185, 36);
            this.reader_label.Name = "reader_label";
            this.reader_label.Size = new System.Drawing.Size(70, 13);
            this.reader_label.TabIndex = 0;
            this.reader_label.Text = "Select reader";
            // 
            // reader_combo
            // 
            this.reader_combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.reader_combo.FormattingEnabled = true;
            this.reader_combo.Location = new System.Drawing.Point(12, 55);
            this.reader_combo.Name = "reader_combo";
            this.reader_combo.Size = new System.Drawing.Size(390, 21);
            this.reader_combo.TabIndex = 2;
            this.reader_combo.SelectedIndexChanged += new System.EventHandler(this.reader_combo_SelectedIndexChanged);
            // 
            // path_box
            // 
            this.path_box.Location = new System.Drawing.Point(93, 12);
            this.path_box.Name = "path_box";
            this.path_box.Size = new System.Drawing.Size(254, 20);
            this.path_box.TabIndex = 3;
            // 
            // path_button
            // 
            this.path_button.Location = new System.Drawing.Point(12, 12);
            this.path_button.Name = "path_button";
            this.path_button.Size = new System.Drawing.Size(75, 20);
            this.path_button.TabIndex = 4;
            this.path_button.Text = "Select folder";
            this.path_button.UseVisualStyleBackColor = true;
            this.path_button.Click += new System.EventHandler(this.path_button_Click);
            // 
            // status_box
            // 
            this.status_box.Location = new System.Drawing.Point(13, 113);
            this.status_box.Name = "status_box";
            this.status_box.ReadOnly = true;
            this.status_box.Size = new System.Drawing.Size(415, 137);
            this.status_box.TabIndex = 5;
            this.status_box.Text = "";
            // 
            // main_loop
            // 
            this.main_loop.Enabled = true;
            this.main_loop.Tick += new System.EventHandler(this.main_loop_Tick);
            // 
            // connect_label
            // 
            this.connect_label.AutoSize = true;
            this.connect_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.connect_label.Location = new System.Drawing.Point(89, 79);
            this.connect_label.Name = "connect_label";
            this.connect_label.Size = new System.Drawing.Size(0, 13);
            this.connect_label.TabIndex = 6;
            // 
            // fixed_reader_label
            // 
            this.fixed_reader_label.AutoSize = true;
            this.fixed_reader_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.fixed_reader_label.Location = new System.Drawing.Point(12, 79);
            this.fixed_reader_label.Name = "fixed_reader_label";
            this.fixed_reader_label.Size = new System.Drawing.Size(71, 13);
            this.fixed_reader_label.TabIndex = 7;
            this.fixed_reader_label.Text = "Connected to";
            // 
            // refresh_button
            // 
            this.refresh_button.BackgroundImage = global::RePers.Properties.Resources.refresh;
            this.refresh_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.refresh_button.Cursor = System.Windows.Forms.Cursors.Default;
            this.refresh_button.Location = new System.Drawing.Point(408, 56);
            this.refresh_button.Name = "refresh_button";
            this.refresh_button.Size = new System.Drawing.Size(20, 20);
            this.refresh_button.TabIndex = 8;
            this.refresh_button.UseVisualStyleBackColor = true;
            this.refresh_button.Click += new System.EventHandler(this.refresh_button_Click);
            // 
            // folder_select
            // 
            this.folder_select.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // parse_button
            // 
            this.parse_button.Location = new System.Drawing.Point(353, 12);
            this.parse_button.Name = "parse_button";
            this.parse_button.Size = new System.Drawing.Size(75, 20);
            this.parse_button.TabIndex = 9;
            this.parse_button.Text = "Parse";
            this.parse_button.UseVisualStyleBackColor = true;
            this.parse_button.Click += new System.EventHandler(this.parse_button_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 262);
            this.Controls.Add(this.parse_button);
            this.Controls.Add(this.refresh_button);
            this.Controls.Add(this.fixed_reader_label);
            this.Controls.Add(this.connect_label);
            this.Controls.Add(this.status_box);
            this.Controls.Add(this.path_button);
            this.Controls.Add(this.path_box);
            this.Controls.Add(this.reader_combo);
            this.Controls.Add(this.reader_label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label reader_label;
        private System.Windows.Forms.ComboBox reader_combo;
        private System.Windows.Forms.TextBox path_box;
        private System.Windows.Forms.Button path_button;
        private System.Windows.Forms.RichTextBox status_box;
        private System.Windows.Forms.Timer main_loop;
        private System.Windows.Forms.Label connect_label;
        private System.Windows.Forms.Label fixed_reader_label;
        private System.Windows.Forms.Button refresh_button;
        private System.Windows.Forms.FolderBrowserDialog folder_select;
        private System.Windows.Forms.Button parse_button;
    }
}

