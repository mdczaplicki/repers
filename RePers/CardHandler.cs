﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GS.Apdu;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;

namespace RePers
{
    public partial class MainForm : Form
    {
        string customer = "NOZ";
        List<string> expected_atr = new List<string>()
        {
            "3B1696D0017ABD0D00", // G213
            "3B1696D00199F80F00", // GTO001
            "3B9E96801FC78031E073FE211B66D0016B040D0063" // MSA098
        };
        /// <summary>
        /// Tuple structure
        /// <paramref name="APDU command" mandatory="True"/>
        /// <paramref name="Expected ATR" mandatory="False"/>
        /// <paramref name="Comment to be written" mandatory="False"/>
        /// </summary>
        TupleList<string, string, string> APDUs = new TupleList<string, string, string>
        {
            {"A0 A4 0000 02 7F45", "", ""},
            {"A0 A4 0000 02 6F42", "", ""},
            {"A0 DC 0104 28 534D532043656E747265FFFFF0038123F4FFFFFFFFFFFFFFFF0791627321912110FFFFFFFF7FF6FF", "9000", ""},
            {"A0 DC 0204 28 534D532043656E747265FFFFF1038134F3FFFFFFFFFFFFFFFF0791627321912110FFFFFFFF7FF6FF", "9000", "Updated TON NPI."},
            {"A0 A4 0000 02 3F00", "", ""},
            {"A0 A4 0000 02 7F45", "", ""},
            {"F0 E0 FF00 10 05885F260C0123250000008383000000", "9000", ""},
            {"A0 A4 0000 02 5F26", "", ""},
            {"A0 DC 0104 23 04686F6D62FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 0204 23 02676FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 0304 23 0468656C70FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 0404 23 046D747278FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 0504 23 0472676373FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 0604 23 046162616CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 0704 23 046370696EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 0804 23 0474707570FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 0904 23 0470626C6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 0A04 23 04626B7477FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 0B04 23 046C737474FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 0C04 23 0461737474FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 0D04 23 0475707464FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 0E04 23 046D636E72FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 0F04 23 0463736869FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1004 23 046373686FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1104 23 0462616C61FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1204 23 046E726763FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1304 23 0470626C32FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1404 23 0370696EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1504 23 0473726567FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1604 23 0472636867FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1704 23 046D636172FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1804 23 0463737267FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1904 23 0465786974FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1A04 23 0464697374FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1B04 23 04736E6463FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1C04 23 0472656761FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1D04 23 047470706DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1E04 23 047470706FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 1F04 23 0464617267FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 2004 23 0175FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 2104 23 0764756D6D793333FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 2204 23 046F626C6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 2304 23 03737462FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 2404 23 0473746261FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", ""},
            {"A0 DC 2504 23 0473746263FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", "Recreated and reinitialized 3F00/7F45/5F26."},
            {"A0 A4 0000 02 3F00", "", ""},
            {"A0 A4 0000 02 7F45", "", ""},
            {"F0 E0 0000 10 01135F76020100009F00000000000000", "9000", ""},
            {"A0 A4 0000 02 5F76", "", ""},
            {"A0 D6 0000 FF 00000000F202FF0000F20063030100015503CD040200052200CF05030005F101D506040007C6011F07050008E502860806000B6B00D20907000C3D00950A08000CD202F30B09000FC5011C0C0A0010E1011C0D0B0011FD00120E0C00120F03A90F0D0015B80284100E00183C0282110F001ABE01731210001C3102FA1311001F2B031E1412002249013D1513002386006216140023E8039D171500278501E7181600296C00E21917002A4E000A1A18002A5803991B19002DF1025B1C1A00304C01E21D1B00322E022E1E1C00345C03791F1D0037D502F6201E003ACB0056211F003B21000AFF20FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", "Recreated and reinitialized 3F00/7F45/5F76."},
            {"A0 A4 0000 02 3F00", "", ""},
            {"A0 A4 0000 02 7F45", "", ""},
            {"80 D4 0000 0A 62088302 6D00 8002 6658", "9000", "Resized 3F00/7F45/6D00"},
            {"A0 A4 0000 02 3F00", "", ""},
            {"A0 A4 0000 02 7F45", "", ""},
            {"A0 A4 0000 02 4F12", "", ""},
            {"A0 D6 0000 01 01", "9000", "Updated 3F00/7F45/4F12."},
            {"A0 A4 0000 02 7F45", "", ""},
            {"A0 A4 0000 02 6E00", "", ""},
            {"A0 DC 0304 32 803F0001085061792042494C4C046F626C6CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", "9000", "Updated URL for Paybill"}
        };

        private string get_iccid()
        {
            reader.Exchange("A0A40000023F00");
            reader.Exchange("A0A40000022FE2");
            RespApdu response = reader.Exchange("A0B000000A", 0x9000);
            byte[] data = response.Data;
            string s_data = BitConverter.ToString(data);
            return Regex.Replace(s_data, "-", "");
        }

        private void authenticate_adm1(string adm1)
        {
            RespApdu response = reader.Exchange("F02A000108" + adm1);
            if (response.SW1SW2 != 0x9000)
                throw new Exception("Error while authenticating.\nExpected SW: 9000.\nReturned: " + ((ushort)response.SW1SW2).ToString("X"));
        }

        private void delete_5F26()
        {
            reader.Exchange("A0 A4 0000 02 3F00");
            reader.Exchange("A0 A4 0000 02 7F45");
            RespApdu response = reader.Exchange("F0 E4 0000 02 5F26");
            if (response.SW1SW2 != 0x9404 && response.SW1SW2 != 0x9000)
                throw new Exception("Errow while deleting 3F00/7F45/5F26.\nExpected SW: 9404 or 9000.\nReturned: " + ((ushort)response.SW1SW2).ToString("X"));
        }

        private void delete_5F76()
        {
            reader.Exchange("A0 A4 0000 02 3F00");
            reader.Exchange("A0 A4 0000 02 7F45");
            RespApdu response = reader.Exchange("F0 E4 0000 02 5F76");
            if (response.SW1SW2 != 0x9404 && response.SW1SW2 != 0x9000)
                throw new Exception("Errow while deleting 3F00/7F45/5F76.\nExpected SW: 9404 or 9000.\nReturned: " + ((ushort)response.SW1SW2).ToString("X"));
        }

        private void modify_ARR_ACC()
        {
            List<RespApdu> responses = new List<RespApdu>();
            responses.Add(reader.Exchange("A0 A4 0000 02 3F00"));
            responses.Add(reader.Exchange("A0 A4 0000 02 2F06"));
            responses.Add(reader.Exchange("A0 DC 0104 64 80011BA010A40683010A950108A40683010B9501088401D49000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"));
            responses.Add(reader.Exchange("A0 DC 0504 64 800103A010A40683010A950108A40683010B950108FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"));
            responses.Add(reader.Exchange("A0 DC 1704 64 800103A010A40683010A950108A40683010B9501088401D4A010A40683010A950108A40683010B950108FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"));
            responses.Add(reader.Exchange("80 D4 0000 0A 620883022F0680020FA0"));
            if (responses[0].SW1SW2 != 0x9F23 || responses[1].SW1SW2 != 0x9F13 || responses[2].SW1SW2 != 0x9000 ||
                responses[3].SW1SW2 != 0x9000 || responses[4].SW1SW2 != 0x9000 || responses[5].SW1SW2 != 0x9000)
                throw new Exception
                (
                    string.Format("Error when modifying ARR_ACC.\n" + 
                                  "Expected SW are:\n9F23, 9F13, 9000, 9000, 9000, 9000.\n" +
                                  "Returned:\n" + 
                                  "{0}, {1}, {2}, {3}, {4}, {5}", ((ushort)responses[0].SW1SW2).ToString("X"),
                                                                  ((ushort)responses[1].SW1SW2).ToString("X"),
                                                                  ((ushort)responses[2].SW1SW2).ToString("X"),
                                                                  ((ushort)responses[3].SW1SW2).ToString("X"),
                                                                  ((ushort)responses[4].SW1SW2).ToString("X"),
                                                                  ((ushort)responses[5].SW1SW2).ToString("X"))
                );
        }
    }
    
    public class FileParser
    {
        private List<string> file_list;
        private Dictionary<string, string> iccid_adm1;

        private string error_message = "";

        public FileParser(){}

        /// <summary>
        /// Make the list of *.OUT files and save it to file_list
        /// </summary>
        /// <param name="dir_path">Path from where *.OUT files will be taken</param>
        public void GetOUTFiles(string dir_path)
        {
            // Make new list
            file_list = new List<string>();
            // Get files in directory
            string[] tmp_list = Directory.GetFiles(dir_path);

            for (int i = 0; i < tmp_list.Length; i++)
                if (tmp_list[i].ToUpper().EndsWith(".OUT"))
                    file_list.Add(tmp_list[i]);

            error_message += "No OUTput files in the folder:\n" + dir_path + "\n";

            //Console.WriteLine(string.Join("\n", file_list.ToArray()));
        }

        /// <summary>
        /// Create a dictionary containing ICCID as key and ADM1 as value
        /// </summary>
        public void ParseOUTFiles()
        {
            // Make new dictionary
            iccid_adm1 = new Dictionary<string, string>();
            for (int i = 0; i < file_list.Count; i++)
            {
                bool var_out = false;
                var lines = File.ReadLines(file_list[i]);
                foreach (string line in lines)
                {
                    if (line.Length == 0)
                        continue;
                    if (var_out)
                    {
                        string iccid = Scramble(GetICCID(line, file_list[i]));
                        string adm1 = StringToAsciiHex(GetADM1(line, file_list[i]));
                        string out_adm1;
                        // Check if there is duplicated ICCID in OUTput files
                        if (iccid_adm1.TryGetValue(iccid, out out_adm1))
                        {
                            iccid_adm1[iccid] = string.Format("{0} {1}", out_adm1, adm1);
                            error_message += string.Format("Duplication of ICCID {0} in file:\n{1}\n", iccid, file_list[i]);
                        }
                        else iccid_adm1.Add(iccid, adm1);
                    }
                    if (!var_out && line.ToUpper().Contains("VAR_OUT"))
                        var_out = true;
                }
            }
            if (error_message.Length != 0)
                throw new Exception(error_message);
        }

        /// <summary>
        /// Get ICCID from line. Find first and second space, take value from between.
        /// </summary>
        /// <param name="line">Line from file</param>
        /// <param name="file_name">File name for bug finding</param>
        /// <returns></returns>
        private string GetICCID(string line, string file_name="")
        {
            int start = line.IndexOf(' '), end = line.IndexOf(' ', start + 1);
            string iccid = line.Substring(start + 1, end - start - 1);
            // If ICCID does not start with 89 then throw an error
            if (!iccid.StartsWith("89"))
                error_message += string.Format("ICCID {0} does not start with 89 in OUTput file:\n{1}\n", iccid, file_name);
                //throw new Exception("ICCID does not start with 89 in OUTput file:\n" + file_name + "\n");
            if (iccid.Length != 20)
                error_message += string.Format("ICCID {0} does not have length equal 20 in OUTput file:\n{1}\n", iccid, file_name);
                //throw new Exception("ICCID does not have length equal 20 in OUTput file:\n" + file_name + "\n");
            Console.WriteLine(error_message);
            return iccid;
        }

        /// <summary>
        /// Get ADM1 from line. Find last space, take value from there till the end.
        /// </summary>
        /// <param name="line">Line from file</param>
        /// <param name="file_name">File name for bug finding</param>
        /// <returns></returns>
        private string GetADM1(string line, string file_name = "")
        {
            int start = line.LastIndexOf(' ');
            string adm1 = line.Substring(start + 1);
            if (adm1.Length != 8)
                error_message += string.Format("ADM1 {0} does not have length equal 8 in OUTput file:\n{1}\n", adm1, file_name);

                //throw new Exception("ADM1 does not have length equal 8 in OUTput file:\n" + file_name + "\n");
            return adm1;
        }

        /// <summary>
        /// "Scramble" - means swap every 2n letter.
        /// </summary>
        /// <param name="data">Data to be swapped</param>
        /// <returns></returns>
        private string Scramble(string data)
        {
            string scrambled = "";
            for (byte i = 0; i < data.Length; i += 2 )
            {
                try
                {
                    scrambled += data[i + 1];
                }
                catch
                {
                    scrambled += "X";
                }
                scrambled += data[i];
            }
            return scrambled;
        }

        /// <summary>
        /// Convert string to ascii hex string. Ex. "001" -> "303031".
        /// </summary>
        /// <param name="data">Data to be converted</param>
        /// <returns></returns>
        public string StringToAsciiHex(string data)
        {
            string ascii = "";
            byte[] bytes = Encoding.Default.GetBytes(data);
            ascii = BitConverter.ToString(bytes);
            ascii = Regex.Replace(ascii, "-", "");
            return ascii;
        }

        public Dictionary<string, string> GetIccidAdm1()
        {
            return iccid_adm1;
        }
    }
    
    /// <summary>
    /// TupleList class is for APDU command list with expected StatusWord
    /// </summary>
    /// <typeparam name="T1">APDU command</typeparam>
    /// <typeparam name="T2">Expected SW</typeparam>
    /// <typeparam name="T3">Info about recieved data</typeparam>6
    public class TupleList<T1, T2, T3> : List<Tuple<T1, T2, T3>>
    {
        public void Add(T1 item1, T2 item2, T3 item3)
        {
            Add(new Tuple<T1, T2, T3>(item1, item2, item3));
        }
    }
}
