﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using GS.Apdu;
using GS.Util;
using GS.PCSC;
using GS.SCard;
using System.Text.RegularExpressions;
using System.Media;
using System.Threading;
using System.Collections.Generic;

namespace RePers
{
    public partial class MainForm : Form
    {
        PCSCReader reader;
        string[] readers;
        string mask = "";
        Item selected;
        BackgroundWorker thread;
        Dictionary<string, string> iccid_adm1 = new Dictionary<string,string>();

        public MainForm()
        {
            InitializeComponent();
            Text = "Reperso Tool for " + customer;

            reader = new PCSCReader();
            set_combo_readers();

            thread = new BackgroundWorker();
            // We cannot modify GUI content from another thread, so we need to report progress which is handled by GUI.
            thread.WorkerReportsProgress = true;
            // We need to support Cancel - if something goes wrong with card then we should stop the worker.
            thread.WorkerSupportsCancellation = true;
            thread.DoWork += new DoWorkEventHandler(
                delegate(object o, DoWorkEventArgs args)
                {
                    BackgroundWorker worker = o as BackgroundWorker;
                    try
                    {
                        reader.ActivateCard(GS.SCard.Const.SCARD_SHARE_MODE.Shared, GS.SCard.Const.SCARD_PROTOCOL.Tx);
                        worker.ReportProgress(0, "Connected to card.\n");

//                      //**********************************************************************************\\
//                      ||                              CARD HANDLING PART                                  ||
//                      \\**********************************************************************************//
                        // Take ATR and check if this is OK for this reperso
                        string card_atr = reader.SCard.AtrString;
                        worker.ReportProgress(0, "ATR " + card_atr + "\n");
                        if (!expected_atr.Contains(Regex.Replace(card_atr, @"\s", "")))
                            throw new Exception("Wrong cards ATR value.\nExpected any of: [ " + string.Join(", ", expected_atr) + " ]\n");
                        if (Regex.Replace(card_atr, @"\s", "") == expected_atr[2])
                            mask = "MSA098";
                        else
                            mask = "GTO/G";
                        // If no ICCID and ADM1 were parsed quit
                        if (iccid_adm1.Count == 0)
                            throw new Exception("Please parse OUTput files.");
                        // Take ICCID from card and retrieve ADM1 from OUTput files
                        string iccid = get_iccid(), adm1;
                        worker.ReportProgress(0, "ICCID " + iccid + "\n");
                        // Try to get ICCID from OUTput files
                        if (!iccid_adm1.TryGetValue(iccid, out adm1))
                            throw new Exception(string.Format("No ICCID {0} in OUTput files.", iccid));
                        worker.ReportProgress(0, "Found ICCID in OUTput files.\n");
                        // Try to authenticate to the card
                        authenticate_adm1(adm1);
                        worker.ReportProgress(0, "Authenticated to the card with ADM1.\n");
                        // Try to delete files
                        delete_5F26();
                        delete_5F76();
                        worker.ReportProgress(0, "Files were deleted.\n");
                        if (mask == "MSA098")
                            modify_ARR_ACC();
                        
                        // Run APDU commands from the list
                        foreach (Tuple<string, string, string> APDU in APDUs)
                        {
                            string apdu = APDU.Item1, status_word = APDU.Item2, comment = APDU.Item3;
                            RespApdu response;
                            if (status_word.Length > 0)
                            {
                                // Change expected SW (string) to nullable unsigned short.
                                ushort? expected = ushort.Parse(status_word, System.Globalization.NumberStyles.HexNumber);
                                // Send APDU command and check if SW is as expected
                                response = reader.Exchange(apdu, expected);
                            }
                            else response = reader.Exchange(apdu);
                            // Write a command if it exists
                            if (comment.Length > 0) worker.ReportProgress(0, comment);
                            try
                            {
                                // Recieve data from card if available
                                byte[] data = response.Data;
                                string s_data = BitConverter.ToString(data);
                                worker.ReportProgress(0, Regex.Replace(s_data, "-", " ") + "\n");
                            }
                            catch (NullReferenceException) { }
                            catch (ArgumentNullException) { }
                            // Make new line if there was a comment
                            if (comment.Length > 0) worker.ReportProgress(0, "\n");
                        }

//                      //**********************************************************************************\\
//                      ||                          END CARD HANDLING PART                                  ||
//                      \\**********************************************************************************//

                        // Make the status_box green
                        worker.ReportProgress(0, "__green");
                        worker.ReportProgress(0, "Please remove card.\n");
                        reader.SCard.WaitForCardRemoval();
                        // Clear the status_box if the card was removed
                        worker.ReportProgress(0, "__blank");
                    }
                    catch (Exception ex)
                    {
                        SystemSounds.Beep.Play();
                        worker.ReportProgress(0, "__red");
                        worker.ReportProgress(0, ex.Message + "\n");
                        worker.ReportProgress(0, "Please remove card.\n");
                        reader.SCard.WaitForCardRemoval();
                        // Clear the status_box if the card was removed
                        worker.ReportProgress(0, "__blank");
                        worker.CancelAsync();
                    }
                });
            thread.ProgressChanged += new ProgressChangedEventHandler(
                delegate(object o, ProgressChangedEventArgs args)
                {
                    if (args.UserState as string == "__green")
                    {
                        status_box.BackColor = Color.LightGreen;
                        return;
                    }
                    else if (args.UserState as string == "__red")
                    {
                        status_box.BackColor = Color.Salmon;
                        return;
                    }
                    else if (args.UserState as string == "__blank")
                    {
                        status_box.Clear();
                        status_box.BackColor = Color.Empty;
                        return;
                    }
                    // Take 2nd argument, make it string and append to status box
                    status_box.Text += args.UserState as string;
                    status_box.SelectionStart = status_box.Text.Length;
                    status_box.ScrollToCaret();
                });
            thread.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
                delegate(object o, RunWorkerCompletedEventArgs args)
                {
                    // Do nothing
                });
        }

        /// <summary>
        /// Run when the combobox selection have changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reader_combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                reader.Disconnect(GS.SCard.Const.SCARD_DISCONNECT.Unpower);
                status_box.Clear();
                // Provide info for worker that the reader was changed and he shouldn't throw errors
                thread.CancelAsync();
                // Take sender (which is combo box) and cast it to (ComboBox), then take selected Item and cast it to (Item)
                selected = ((ComboBox)sender).SelectedItem as Item;
                // Connect to selected reader. We use Value to reference a list of reader to be sure a right one is used.
                reader.Connect(readers[selected.Value], GS.SCard.Const.SCARD_SCOPE.User);
                reader.SCard.EstablishContext(GS.SCard.Const.SCARD_SCOPE.User);
                connect_label.Text = readers[selected.Value];
            }
            catch (Exception ex)
            {
                status_box.Text += ex.Message;
            }
        }

        /// <summary>
        /// Select path of directory and print it to text box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void path_button_Click(object sender, EventArgs e)
        {
            DialogResult result = folder_select.ShowDialog();
            if (result == DialogResult.OK)
            {
                path_box.Text = folder_select.SelectedPath;
            }            
        }

        /// <summary>
        /// Tick occurs every 100ms.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void main_loop_Tick(object sender, EventArgs e)
        {
            if (reader.SCard.IsRMContextEstablished)
            {
                if (!thread.IsBusy)
                    thread.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Refresh the list of readers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void refresh_button_Click(object sender, EventArgs e)
        {
            set_combo_readers();
        }
        
        /// <summary>
        /// Method used for retrieving reader list and filling combobox with them.
        /// </summary>
        private void set_combo_readers()
        {
            reader.Disconnect(GS.SCard.Const.SCARD_DISCONNECT.Unpower);
            status_box.Clear();
            status_box.BackColor = Color.Empty;
            reader_combo.Items.Clear();
            reader_label.Text = "";
            reader.SCard.EstablishContext();
            readers = reader.SCard.ListReaders();
            reader.SCard.ReleaseContext();

            for (byte i = 0; i < this.readers.Length; i++)
                reader_combo.Items.Add(new Item(readers[i], i));
        }

        /// <summary>
        /// Runs parser class and creates a dictionary of <ICCID, ADM1>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void parse_button_Click(object sender, EventArgs e)
        {
            string path = path_box.Text;
            bool error = false;
            try
            {
                FileParser file_parser = new FileParser();
                file_parser.GetOUTFiles(path);
                try
                {
                    file_parser.ParseOUTFiles();
                }
                catch (Exception ex)
                {
                    error = true;
                    status_box.BackColor = Color.Orange;
                    status_box.Text += ex.Message + "You can continue reworking cards.\n";
                }
                iccid_adm1 = file_parser.GetIccidAdm1();
                status_box.Text += error ? "Parsed OUTput files with errors.\n" : "Successfully parsed OUTput files.\n";
                status_box.BackColor = error ? Color.PeachPuff : Color.LightGreen;
            }
            catch (Exception ex)
            {
                status_box.BackColor = Color.Salmon;
                status_box.Text += ex.Message + "\n";
            }
        }
    }


    /// <summary>
    /// Class used for combo box, with a name and unique id (value) just to show the name.
    /// </summary>
    public class Item
    {
        public string Name;
        // Save memory - always.
        public byte Value;
        public Item(string name, byte value)
        {
            this.Name = name;
            this.Value = value;
        }
        /// <summary>
        /// Let's override ToString() so that combo box will not print Value as well.
        /// </summary>
        /// <returns>Name of item</returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}
