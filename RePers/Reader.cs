﻿/*
Copyright (c) 2011, Gerhard H. Schalk, www.smartcard-magic.net
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/
/*using System;
using System.Diagnostics;
using GS.SCard;


namespace ExampleSCard
{
    class Program
    {
        static void Main( string[] args )
        {   

            ConsoleTraceListener consoleTraceListener = new ConsoleTraceListener();
            Trace.Listeners.Add(consoleTraceListener);

            WinSCard scard = new WinSCard();
            
            try
            {
                scard.EstablishContext();
                scard.ListReaders();
               
                Console.Write( "Please select a reader (0..n):  " );
                string readerName = scard.ReaderNames[int.Parse(Console.ReadLine())];


                Console.Write("Wait for card present...\n");
                scard.WaitForCardPresent(readerName);
                scard.Connect(readerName);

                //Console.WriteLine("ATR: 0x" + scard.AtrString);
                
                
                
                byte[] cmdApdu = { 0xFF, 0xCA, 0x00, 0x00, 00 }; // Get Card UID ...
                //byte[] cmdApdu = { 0xF0, 0x2A, 0x00, 0x0F, 0x08 };
                byte[] respApdu = new byte[256];
                
                int respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                Console.WriteLine("\n\n\n");
                Console.WriteLine(respLength);
                Console.WriteLine("\n\n\n");

            }
            catch (WinSCardException ex)
            {
                Console.WriteLine( ex.WinSCardFunctionName + " 0x" + ex.Status.ToString( "X08" ) + " " + ex.Message );
            }
            finally
            {
                scard.Disconnect();
                scard.ReleaseContext();
                Console.WriteLine("Please press any key...");
                Console.ReadLine();
            }            
        }
    }
}
*/
/*using System;
using System.Diagnostics;
using GS.Apdu;
using GS.PCSC;
using GS.SCard;
using GS.SCard.Const;
using GS.Util.Hex;

namespace RePers
{
    class Reader
    {
        ConsoleTraceListener consoleTraceListener = new ConsoleTraceListener();
        Trace.Listeners.Add(consoleTraceListener);

        PCSCReader reader = new PCSCReader();

        try
        {

            reader.Connect();
            reader.ActivateCard();

            RespApdu respApdu = reader.Exchange("F0 2A 00 0F 08 7B2ED9F17BF42CBC"); // Get Card UID ...
            respApdu = reader.Exchange("00 A4 00 04 02 3F 00"); // Get Card UID ...
            respApdu = reader.Exchange("00 A4 00 04 02 2F E2"); // Get Card UID ...
            respApdu = reader.Exchange("00 D6 00 00 0A 22222222222222222222"); // Get Card UID ...
            if (respApdu.SW1SW2 == 0x9000)
            {
                Console.WriteLine("ICCID  = 0x" + HexFormatting.ToHexString(respApdu.Data, true));
            }
        }
        catch (WinSCardException ex)
        {
            Console.WriteLine(ex.WinSCardFunctionName + " Error 0x" +
                                ex.Status.ToString("X08") + ": " + ex.Message);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
        finally
        {
            reader.Disconnect();
            Console.WriteLine("Please press any key...");
            Console.ReadLine();
        }
    }
}*/